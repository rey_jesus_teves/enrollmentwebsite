const artyom = new Artyom();

artyom.initialize({ lang: "en-US", debug:true});

function talk(element, message){
document.getElementById(element).addEventListener(
    "click",
    () => {

        artyom.shutUp();

        artyom.say(message, { lang: "en-US" });


    });
}

// heading
{
    let element = "heading"
    let message = "Rey O E D and Tutorials. Rey's Online Education and Tutorials. Book courses online. From Basic English to Web Development."
    talk(element, message );
}

// card 1
{
    let element = "card1"
    let message = "We offer courses, from Basic English Grammar, to, Conversational, English!"
    talk(element, message );
}

// card 2
{
    let element = "card2"
    let message = "We offer courses in HTML, CSS, Bootstrap and Javascript."
    talk(element, message );
}

// card 3
{
    let element = "card3"
    let message = "We offer courses in MongoDB, Express and Node JS."
    talk(element, message );
}


