let adminUser = localStorage.getItem('isAdmin') 
let token = localStorage.getItem('token')
let profile = document.querySelector('#profileInfo')
let enrolledCourses = document.querySelector('#userEnrolledCourses')
let profileContainer = document.querySelector('#profileContainer')
let editProfileButton = document.querySelector('#editProfileButton')
let enrolledHeader = document.querySelector('#enrolledHeader')

if (adminUser === "false"){
	let id = localStorage.getItem('id')

	fetch('https://rocky-harbor-60941.herokuapp.com/api/users/details',{
		method: "GET",
		headers: {
			"Authorization": `Bearer ${token}`
		}
	})
	.then(res=>res.json())
	.then(data=>{


		profile.innerHTML = 
		`
			<div class="col-sm-12 text-center card py-3">
				<h4 class="py-3 card-header">${data.firstName} ${data.lastName}</h4>
				<p class="pb-0 mb-0"></p>
				<p class="card-text mt-3">Mobile Number: ${data.mobileNo}</p>
				<p class="card-text mt-3">Email Address: ${data.email}</p>
			</div>
		`

		

		data.enrollments.map(courses=>{
			fetch(`https://rocky-harbor-60941.herokuapp.com/api/courses/${courses.courseId}`)
			.then(res=>res.json())
			.then(data2=>{ 



				console.log(data2);

				// display only active courses
				if(data2.isActive==true){

				enrolledHeader.innerHTML = `Courses Enrolled`;

				return ( 



					enrolledCourses.innerHTML +=
					`
						

						<div class="col-md-6 my-3">
							<div class="card">
								<div class="card-body shadow border border-secondary" id="outsideOfProfileCourseCard">
									<p class="card-header font-weight-bold text-center" id="profileCourseTitle">${data2.name}</p>
									<p class="card-text font-weight-bold text-center mt-3 mb-1" id="profileCourseTitle">${data2.description}</p>
				
								</div>
							</div>
						</div>
					`
			
				)
											// end of isActive
											}

			})
		})
	})	
} else {
	profileContainer.innerHTML = ``
}