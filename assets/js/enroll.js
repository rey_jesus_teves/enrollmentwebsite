const courseSearch = document.querySelector('#courseSearch');
let access = localStorage.getItem('token')

const queryId = window.location.search;
const urlParams = new URLSearchParams(queryId);
const id = urlParams.get('id')

const courseId = document.querySelector('#id');
const name = document.querySelector('#name');
const desc = document.querySelector('#desc');
const price = document.querySelector('#price');

fetch(`https://rocky-harbor-60941.herokuapp.com/api/courses/${id}`, {
        headers: {
            Authorization: `Bearer ${access}`
        }
    })
    .then(res => res.json())
    .then(course => {


        courseId.innerText = `Course ID: ` + `${course._id}`
        name.innerText = `Course Name: ${course.name}`
        desc.innerText = `Description: ${course.description}`
        price.innerText = `Price: ₱ ${course.price}`

    })


courseSearch.addEventListener('submit', (e) => {
    e.preventDefault()

    const courseId = document.querySelector('#course').value

    if (courseId !== '') {
        fetch('https://rocky-harbor-60941.herokuapp.com/api/users/enroll', {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${access}`,
                    'Content-Type': 'application/json',
                },

                body: JSON.stringify({
                    courseId: courseId,



                })

            })
            .then(res => res.json())
            .then(data => {
                
                Swal.fire({
                    icon: 'success',
                    title: 'Good Job',
                    text: 'Enrollment Successful!',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    }

                }).then(function() {
            window.location.href = "courses.html";
        })

                
            })

} else {
        Swal.fire({
            icon: 'info',
            title: 'Instruction.',
            text: 'Enter the Course ID in the input box to confirm enrollment.',
            showClass: {
                popup: 'animate__animated animate__fadeInDown'
            },
            hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
            }

        })

    }
})