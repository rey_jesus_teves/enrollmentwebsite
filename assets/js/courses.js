let token = localStorage.getItem('token');
let adminUser = localStorage.getItem('isAdmin');
let courseContainer = document.querySelector('#coursesContainer');
let buttonControl = document.querySelector('#buttonControl');
let courseData;
let cardFooter;

// Check if logged in

if (!token || token === null){
    
buttonControl.innerHTML = `<a href="../index.html" class="btn btn-primary mt-3 buttonEffect">
      Home Page </a>`;

fetch('https://rocky-harbor-60941.herokuapp.com/api/courses', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
        }

    })

    .then(res => res.json())
    .then(data => {

        if (data.length<1){
            courseData = "No Courses Available."
        };

        courseData = data.reverse().map(courseData => {
            
            cardFooter = `<a href="./register.html" class="btn btn-primary text-white btn-block">Sign Up and Subscribe</a>`;

            return (
                `
                <div class="col-md-6 my-3">
                        <div class="card shadow border border-secondary">
                            <div class="card-body">
                                <h5 class="card-title">${courseData.name}</h5>
                                <p class="card-text text-left">${courseData.description}</p>
                                <p class="card-text text-right">&#8369;${courseData.price}</p>
                            </div>
                            <div class="card-footer">${cardFooter}</div>
                        </div>
                    </div> 
                `)
        }).join("");


        courseContainer.innerHTML = courseData;
    })


// End of if logged in
} 

else {

// If logged in as Admin

if (adminUser === "true") {

alert("Please Contact Web Developer to request Admin Access.");

window.location.replace('../index.html');
	
// If logged in as Student

} else {
	


	buttonControl.innerHTML = `<a href="./profile.html" class="btn btn-primary mt-3 buttonEffect">
      View Profile </a>`;

	fetch('https://rocky-harbor-60941.herokuapp.com/api/courses', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
        }

    })

    .then(res => res.json())
    .then(data => {

    	if (data.length<1){
			courseData = "No Courses Available."
		};

        courseData = data.reverse().map(courseData => {
            
        	cardFooter = `<a href="./course.html?courseId=${courseData._id}" class="btn btn-primary text-white btn-block">Go To Course</a>`;

            return (
                `
				<div class="col-md-6 my-3">
						<div class="card shadow border border-secondary">
							<div class="card-body">
								<h5 class="card-title">${courseData.name}</h5>
								<p class="card-text text-left">${courseData.description}</p>
								<p class="card-text text-right">&#8369;${courseData.price}</p>
							</div>
							<div class="card-footer">${cardFooter}</div>
						</div>
					</div> 
				`)
        }).join("");


        courseContainer.innerHTML = courseData;
    })


// End of If Student
}

// End of else logged In
}