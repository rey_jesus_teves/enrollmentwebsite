let params = new URLSearchParams(window.location.search); 
let courseId = params.get('courseId');
let token = localStorage.getItem('token');

console.log("courseId: " + courseId);

let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")

fetch(`https://rocky-harbor-60941.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price

	if (isAdmin == "false") {


// if already enrolled tell: You are enrolled in this course.

fetch('https://rocky-harbor-60941.herokuapp.com/api/users/details', {
        method: "GET",
        headers: {
            "Authorization": `Bearer ${token}`
        }
    })
    .then(res => res.json())
    .then(userData => {



        userData.enrollments.map(coursesEnrolled => {


            let courseidsEnrollled = coursesEnrolled.courseId;



            if (courseidsEnrollled.includes(data._id)) {

                enrollContainer.innerHTML = `<h5 class="text-white card-footer">You are enrolled in this course.  </h5>`

            }



        });
        // End Get User Data 
    })


// If unenrolled show enroll button
		            
		enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`

		document.querySelector("#enrollButton").addEventListener("click",()=>{

			fetch('https://rocky-harbor-60941.herokuapp.com/api/users/enroll',{
				method:'POST',
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					courseId: courseId

				})
			})
			.then(res => res.json())
			.then(data => {


				if(data){
					alert("You have enrolled successfully.")
					window.location.replace("./courses.html")
				} else {
					alert("Enrollment Failed.")
				}
			})
		})

	} 

	// if Admin login

	else if (isAdmin == "true") { 

	alert("Please Contact Web Developer to request Admin Access.");

	window.location.replace('../index.html');

	}
})
